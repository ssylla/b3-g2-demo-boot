package com.example.demo.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class AppSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Override
	protected void configure( AuthenticationManagerBuilder builder ) throws Exception {
		
		builder.inMemoryAuthentication()
			   .withUser( "sega" ).password( "{noop}sylla" ).roles( "USER" )
			   .and()
			   .withUser( "admin" ).password( "{noop}adminsega" ).roles( "USER", "HERO" );
	}
}
