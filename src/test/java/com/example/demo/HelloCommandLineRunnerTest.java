package com.example.demo;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.system.CapturedOutput;
import org.springframework.boot.test.system.OutputCaptureExtension;
import org.springframework.test.context.TestPropertySource;

import static org.assertj.core.api.BDDAssertions.then;

@SpringBootTest
@ExtendWith( OutputCaptureExtension.class )
@TestPropertySource(properties = {"service.hello.prefix=Guten tag"})
class HelloCommandLineRunnerTest {
	
	@Test
	void testRun( CapturedOutput output ) {
		then(output.getOut()).contains( "Hello " );
	}
}