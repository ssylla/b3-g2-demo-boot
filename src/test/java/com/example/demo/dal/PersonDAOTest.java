package com.example.demo.dal;

import com.example.demo.domain.Person;
import org.hibernate.mapping.Set;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class PersonDAOTest {
	
	@Autowired
	private PersonDAO dao;
	
	@Test
	public void testSave() {
		//Preparation
		Person person = new Person( "E S.", "esylla@ss.org", "@esylla" );
		
		//Action
		Person savedPerson = dao.save( person );
		
		//Excepting
		assertNotEquals( savedPerson.getId(), null );
		// assertNotNull( savedPerson.getId() );
		// assertEquals( person.hashCode(), savedPerson.hashCode() );
		
		// List<Person> list = new ArrayList<>();
		// list.add( person );
		// list.add( savedPerson );
		//
		// java.util.Set<Person> set = new HashSet<>();
		// set.add( person );
		// set.add( savedPerson );
		//
		// assertEquals( list.size(), set.size() );
		
	}
	
	@Test
	void testFindByTwitter() {
		
		//Préparation
		Person person = new Person( "M S.", "msylla@ss.org", "@msylla" );
		
		//Action
		dao.save( person );
		
		//Excepting
		assertEquals( person.getTwitter(), dao.findByTwitter( person.getTwitter() ).getTwitter() );
	}
	
	
	public void cleanContext() {
	
	}
}