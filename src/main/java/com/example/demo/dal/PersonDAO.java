package com.example.demo.dal;

import com.example.demo.domain.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Collection;

@RepositoryRestResource( path = "salaries", collectionResourceRel = "mesSalaries" )
public interface PersonDAO extends CrudRepository<Person, Long> {
	
	@RestResource( path = "by-twitter" )
	Person findByTwitter( @Param( "id" ) String twitter );
	
	Person findByEmailAndTwitter( String email, String twitter );
	
	Collection<Person> findByName( String name );
}
