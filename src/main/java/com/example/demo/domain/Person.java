package com.example.demo.domain;

import org.springframework.boot.autoconfigure.domain.EntityScan;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Person implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	private String email;
	@Column(unique = true)
	private String twitter;
	
	public Person() {}
	
	public Person( String name, String email, String twitter ) {
		this.name = name;
		this.email = email;
		this.twitter = twitter;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId( Long id ) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName( String name ) {
		this.name = name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail( String email ) {
		this.email = email;
	}
	
	public String getTwitter() {
		return twitter;
	}
	
	public void setTwitter( String twitter ) {
		this.twitter = twitter;
	}
	
	@Override
	public boolean equals( Object o ) {
		if ( this == o ) return true;
		if ( !(o instanceof Person) ) return false;
		Person person = ( Person ) o;
		return Objects.equals( id, person.id ) && Objects.equals( name, person.name );
	}
	
	@Override
	public int hashCode() {
		return Objects.hash( id, name );
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Person{" );
		sb.append( "id=" ).append( id );
		sb.append( ", name='" ).append( name ).append( '\'' );
		sb.append( ", email='" ).append( email ).append( '\'' );
		sb.append( ", twitter='" ).append( twitter ).append( '\'' );
		sb.append( '}' );
		return sb.toString();
	}
}
