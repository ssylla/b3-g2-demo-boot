package com.example.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class HelloCommandLineRunner implements CommandLineRunner {
	
	@Value( "${service.hello.prefix:bonjour}" )
	private String prefix;
	@Value( "${service.hello.suffix:!!!}" )
	private String suffix;
	
	@Override
	public void run( String... args ) throws Exception {
		
		System.out.printf( "%s, %s%s%n", prefix, "B3", suffix );
	}
}
